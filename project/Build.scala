import com.typesafe.sbt.SbtPgp.autoImportImpl.{ pgpPassphrase, PgpKeys }
import sbt.Keys._
import sbt._
import sbt.plugins.JvmPlugin
import sbtrelease.ReleasePlugin.autoImport._

object Build extends AutoPlugin {

  override def requires: sbt.Plugins = JvmPlugin

  override def trigger = allRequirements

  override def projectSettings: Seq[sbt.Def.Setting[_]] =
    Vector(
      // Core settings
      organization := "org.make.constructr",
      scalaVersion := Version.scala213,
      crossScalaVersions := Vector(Version.scala212, Version.scala213),
      scalacOptions ++= Vector(
        "-unchecked",
        "-deprecation",
        "-language:_",
        "-target:jvm-1.8",
        "-encoding",
        "UTF-8"
      ),
      licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0")),
      homepage := Some(url("https://gitlab.com/makeorg/devtools/constructr-zookeeper")),
      developers := List(
        Developer("flaroche", "François LAROCHE", "fl@make.org", url("https://github.com/larochef"))
      ),
      scmInfo := Some(
        ScmInfo(
          browseUrl = url("https://gitlab.com/makeorg/devtools/constructr-zookeeper"),
          connection = "scm:git:git://gitlab.com:makeorg/devtools/constructr-zookeeper.git",
          devConnection = Some("scm:git:ssh://gitlab.com:makeorg/devtools/constructr-zookeeper.git")
        )
      ),
      pomIncludeRepository := (_ => false),
      publishMavenStyle := true,
      releasePublishArtifactsAction := PgpKeys.publishSigned.value,
      pgpPassphrase := {
        val password: String = System.getenv("GPG_PASSWORD")
        password match {
          case null =>
            None
          case "" =>
            None
          case other =>
            Some(other.trim.toCharArray)
        }
      },
      publishTo := {
        if (isSnapshot.value) {
          Some("releases".at("https://oss.sonatype.org/content/repositories/snapshots"))
        } else {
          Some("snapshots".at("https://oss.sonatype.org/service/local/staging/deploy/maven2"))
        }
      },
      unmanagedSourceDirectories.in(Compile) := Vector(scalaSource.in(Compile).value),
      unmanagedSourceDirectories.in(Test) := Vector(scalaSource.in(Test).value),
      // Release settings
      releaseCrossBuild := true

      // FIXME
      // For some reason, +test results in tests failing due to bincompat issues, explicitly
      // issuing clean seems to work around it for now
    ) ++ addCommandAlias("run-tests",
                         s";++${Version.scala213};clean;test;++${Version.scala212};clean;test")
}
