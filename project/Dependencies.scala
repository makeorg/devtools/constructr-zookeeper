import sbt._

object Version {
  val akka          = "2.5.25"
  val constructr    = "0.20.0"
  val curator       = "2.13.0"
  val scala212      = "2.12.10"
  val scala213      = "2.13.1"
  val scalaTest     = "3.0.8"
  val dockerItScala = "0.9.9"
}

object Library {
  val akkaActor              = "com.typesafe.akka"   %% "akka-actor"                      % Version.akka
  val akkaCluster            = "com.typesafe.akka"   %% "akka-cluster"                    % Version.akka
  val akkaMultiNodeTestkit   = "com.typesafe.akka"   %% "akka-multi-node-testkit"         % Version.akka
  val akkaTestkit            = "com.typesafe.akka"   %% "akka-testkit"                    % Version.akka
  val constructr             = "org.make.constructr" %% "constructr"                      % Version.constructr
  val constructrCoordination = "org.make.constructr" %% "constructr-coordination"         % Version.constructr
  val curatorFramework       = "org.apache.curator"  % "curator-framework"                % Version.curator
  val curatorRecipes         = "org.apache.curator"  % "curator-recipes"                  % Version.curator
  val scalaTest              = "org.scalatest"       %% "scalatest"                       % Version.scalaTest
  val dockerTestKit          = "com.whisk"           %% "docker-testkit-scalatest"        % Version.dockerItScala
  val dockerTestKitImpl      = "com.whisk"           %% "docker-testkit-impl-docker-java" % Version.dockerItScala
}
